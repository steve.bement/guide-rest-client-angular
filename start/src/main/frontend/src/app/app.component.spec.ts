import { TestBed, async } from '@angular/core/testing';
import { AppComponent, ArtistsService } from './app.component';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { HttpClient, HttpErrorResponse, HttpHandler } from '@angular/common/http';

describe('AppComponent without TestBed', () => {
  let component: AppComponent;
  let httpClientSpy: jasmine.SpyObj<HttpClient>;
  let service = new ArtistsService(httpClientSpy);
  var fetchSpy;

  beforeEach(() => {
    component = new AppComponent(service);
    fetchSpy = spyOn(service, 'fetchArtists').and.returnValue(Promise.resolve());
  });

  it('should create the app', () => {
    expect(component).toBeTruthy();
  });

  it('should invoke service from ngOnInit', () => {
    component.ngOnInit();
    expect(fetchSpy.calls.count()).toBe(1);
  });
});

describe('AppComponent', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        AppComponent
      ],
      providers: [
        HttpClient, HttpHandler
      ]
    }).compileComponents();
  }));

  it('should create the app', () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  });

  it(`should have as title 'frontend'`, () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app.title).toEqual('Frontend');
  });

  it('should render title in a h1 tag', () => {
    const fixture = TestBed.createComponent(AppComponent);
    fixture.detectChanges();
    const compiled = fixture.debugElement.nativeElement;
    expect(compiled.querySelector('h1').textContent).toContain('Welcome to frontend!');
  });
});
